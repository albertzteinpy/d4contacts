# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ContactFiles'
        db.create_table(u'appcontact_contactfiles', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type_file', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('contact_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'appcontact', ['ContactFiles'])

        # Adding model 'Contact'
        db.create_table(u'appcontact_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('userrelated', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
            ('usercreator', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('namec', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('lastname', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('chargec', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('date_create', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, auto_now_add=True, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default=1, max_length=2)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('delomun', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('colonia', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('cp', self.gf('django.db.models.fields.CharField')(default=100, max_length=10, null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(default=' ', max_length=200, null=True, blank=True)),
            ('lon', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('lat', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('extrac', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('photoc', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('tags', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'appcontact', ['Contact'])

        # Adding model 'MetaContact'
        db.create_table(u'appcontact_metacontact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('asignetstring', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('asignetto', self.gf('django.db.models.fields.IntegerField')()),
            ('metastring', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('metavalue', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('contact_meta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appcontact.Contact'])),
        ))
        db.send_create_signal(u'appcontact', ['MetaContact'])

        # Adding model 'EmailList'
        db.create_table(u'appcontact_emaillist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contacto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appcontact.Contact'])),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75)),
            ('typeofemail', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
        ))
        db.send_create_signal(u'appcontact', ['EmailList'])

        # Adding model 'Tag'
        db.create_table(u'appcontact_tag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tagname', self.gf('django.db.models.fields.CharField')(default='Untitle', max_length=200, null=True)),
        ))
        db.send_create_signal(u'appcontact', ['Tag'])

        # Adding model 'Sepomex'
        db.create_table(u'appcontact_sepomex', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo_postal', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('asentamiento', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('tipo_asentamiento', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('municipio', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ciudad', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('descripcion_codigo_postal', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('clave_estado', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('clave_oficina', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('clave_tipo_asentamiento', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('clave_municipio', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('clave_asentamiento', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('zona', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('clave_ciudad', self.gf('django.db.models.fields.CharField')(max_length=2)),
        ))
        db.send_create_signal(u'appcontact', ['Sepomex'])

        # Adding model 'Note'
        db.create_table(u'appcontact_note', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')(default='untitle')),
            ('sender', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('receptor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appcontact.Contact'])),
            ('date_create', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'appcontact', ['Note'])

        # Adding model 'ExtraFields'
        db.create_table(u'appcontact_extrafields', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fieldname', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('typefield', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal(u'appcontact', ['ExtraFields'])

        # Adding model 'OptionFields'
        db.create_table(u'appcontact_optionfields', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fieldp', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appcontact.ExtraFields'])),
            ('optiontxt', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'appcontact', ['OptionFields'])


    def backwards(self, orm):
        # Deleting model 'ContactFiles'
        db.delete_table(u'appcontact_contactfiles')

        # Deleting model 'Contact'
        db.delete_table(u'appcontact_contact')

        # Deleting model 'MetaContact'
        db.delete_table(u'appcontact_metacontact')

        # Deleting model 'EmailList'
        db.delete_table(u'appcontact_emaillist')

        # Deleting model 'Tag'
        db.delete_table(u'appcontact_tag')

        # Deleting model 'Sepomex'
        db.delete_table(u'appcontact_sepomex')

        # Deleting model 'Note'
        db.delete_table(u'appcontact_note')

        # Deleting model 'ExtraFields'
        db.delete_table(u'appcontact_extrafields')

        # Deleting model 'OptionFields'
        db.delete_table(u'appcontact_optionfields')


    models = {
        u'appcontact.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'default': "' '", 'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'chargec': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'colonia': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'cp': ('django.db.models.fields.CharField', [], {'default': '100', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'delomun': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'extrac': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastname': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'lon': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'namec': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'photoc': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '2'}),
            'tags': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'usercreator': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'userrelated': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'appcontact.contactfiles': {
            'Meta': {'object_name': 'ContactFiles'},
            'contact_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type_file': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'appcontact.emaillist': {
            'Meta': {'object_name': 'EmailList'},
            'contacto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appcontact.Contact']"}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'typeofemail': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'})
        },
        u'appcontact.extrafields': {
            'Meta': {'object_name': 'ExtraFields'},
            'fieldname': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'typefield': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appcontact.metacontact': {
            'Meta': {'object_name': 'MetaContact'},
            'asignetstring': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'asignetto': ('django.db.models.fields.IntegerField', [], {}),
            'contact_meta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appcontact.Contact']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metastring': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'metavalue': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appcontact.note': {
            'Meta': {'ordering': "['date_create']", 'object_name': 'Note'},
            'date_create': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'receptor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appcontact.Contact']"}),
            'sender': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'text': ('django.db.models.fields.TextField', [], {'default': "'untitle'"})
        },
        u'appcontact.optionfields': {
            'Meta': {'object_name': 'OptionFields'},
            'fieldp': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appcontact.ExtraFields']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'optiontxt': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appcontact.sepomex': {
            'Meta': {'object_name': 'Sepomex'},
            'asentamiento': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'ciudad': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'clave_asentamiento': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'clave_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'clave_estado': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'clave_municipio': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'clave_oficina': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'clave_tipo_asentamiento': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'codigo_postal': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'descripcion_codigo_postal': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'municipio': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'tipo_asentamiento': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'zona': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'appcontact.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tagname': ('django.db.models.fields.CharField', [], {'default': "'Untitle'", 'max_length': '200', 'null': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['appcontact']