#z -* encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response,render
from contactmanager.forms import *
from appcontact.models import * 
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
from django.contrib.auth import logout
import string as STrg
from django.db.models import Q
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

ESTADOS = [('','------------Seleccione-----------'),]
ESTADOS += [(x.estado,x.estado) for x in Sepomex.objects.all().distinct('estado')]

# ********************************** DRY METHODS    ******************************************************


def abc_tag_list():
    """ 
        Método para listar las tags contenidas en el modelo tag
    """
    #: No recibe parametros
    #: retorna una tupla iterable con los tags contenidos en el modelo
    a = {x:[] for x in STrg.ascii_uppercase}
    for x in range(0,10):
        a.update({x:[]})
    #a. (x for x in range(0,9))
    
    T = Tag.objects.all()
    for x in T:
        #c = Contact.objects.filter(tags__icontains='%s'%(x.tagname))
        #en = None #Entity.objects.filter(tags__icontains='%s'%(x.tagname))
        if x.tagname:
            try: 
                a['%s'%(x)].append({'tag':'%s'%(x.tagname)})
            except:
                pass
    return a

# VIEWS TO "THE SISTEMA" ----------------------------------------------------------


def index(request):
    context = RequestContext(request)
    user = request.user
    if user.is_active:
        return redirect('/inicio')
    mensaje = request.GET.get('mensaje',None)
    return render_to_response("login.html",{'mensaje':mensaje},context)


@login_required(login_url="/")
def inicio(request):
    return render(request,"inicio.html",{'a':'a'})

@login_required(login_url='/')
def contact_form(request):
    """
        método para generar el formualrio de contactos
    """

    # : En esta parte el metodo valdia si la llamada fué por el medio de ajax o bien una llamada normal de navegador 
    if request.is_ajax():
        INDEX_EXTEND = 'blank.html'
    # : cuando la llamada se ahce por ajax el tmeplate se extiende de  un template en blanco ya que la respuesta 
    #   Se colocara en un "modal"
    else:
        INDEX_EXTEND = 'base2.html'
    states=None
    modelo = Contact
    forma = FormCreator()
    #Instanceamos el formulario
        #los campos que deseamos exluir
    excludes = ('usercreator','userrelated_id','status','date_create','phones','tags')
        # cambiamos el aspecto de algunos campos con el parametro WIDGET de django Models
    widgets = {
                'estado':forms.Select(choices=ESTADOS,attrs={'class':'sensao'}),
                'delomun':forms.Select(),
                'colonia':forms.Select(),
                'cp':forms.Select(),
                'lat':forms.HiddenInput(),
                'lon':forms.HiddenInput(),
                'photoc':forms.HiddenInput()
              }
    #Construimos el formulario con sus parámetros: 
    forma = forma.advanced_form_to_model(modelo=modelo,excludes=excludes,widgets=widgets)
    extras = ExtraFields.objects.all().order_by('ordering') 
    """
        En caso de existir campos extra par ael formualrio deo contactos se construye un formulario dinámico
        primero se extraen lso datso de la BD ordenados como lo indica en modelo, e sest ecaso por el campo ordering
        se hace instacne de la clase DynamicForm
    """
    extraform = DynamicForm()
    extraform = extraform.from_model(extras)
    # Esta clase recibe como parametro el listado de objetos extraiodos de la base de datos y contruye de manera dinamicoa el formualrio
    # Por el momento solo se cuentan con dos tipos de campos para construir el formulario 
    contactform=[Formcontact()]
    rsform=[Formcontact()]
    addsform = [Formcontact()]
    return render(request,'forms/eform.html',
                              {'lista':'forms_list',
                              'extraform':extraform,
                              'forma':forma,
                              'cform':contactform,
                              'rsform':rsform,
                              'addsform':addsform,
                              'states':states,'extend':INDEX_EXTEND})


@login_required(login_url='/')
def edtcontact(request,idc=None):
    """
        
    """

    
    if idc:
        c = Contact.objects.get(pk=idc)
    else:
        return HttpResponse('You need an ID')

    if request.is_ajax():
        INDEX_EXTEND = 'blank.html'
    else:
        INDEX_EXTEND = 'base2.html'

    context = RequestContext(request)
    states=None
    modelo = Contact
    forma = FormCreator()
    excludes = ('status','date_create','phones','tags')
    municipios = Sepomex.objects.filter(estado=c.estado).distinct('municipio').values('municipio')
    colonias = Sepomex.objects.filter(municipio=c.delomun).distinct('asentamiento').values('asentamiento')
    cps = Sepomex.objects.filter(asentamiento=c.colonia,
                                 municipio=c.delomun,
                                 estado=c.estado).distinct('codigo_postal').values('codigo_postal')
    
    MUNICIPIO = [('','----seleccione-----------')]
    COLONIA = [('','----seleccione-----------')]
    CP = [('','----seleccione-----------')]
    MUNICIPIO += [(x['municipio'],x['municipio']) for x in municipios]
    COLONIA += [(x['asentamiento'],x['asentamiento']) for x in colonias]
    CP += [(x['codigo_postal'],x['codigo_postal']) for x in cps]
    widgets = {
                'estado':forms.Select(choices=ESTADOS,attrs={'class':'sensao'}),
                'delomun':forms.Select(choices=MUNICIPIO),
                'colonia':forms.Select(choices=COLONIA),
                'cp':forms.Select(choices=CP),
                'lat':forms.HiddenInput(),
                'lon':forms.HiddenInput(),
                'photoc':forms.HiddenInput(),
                'usercreator':forms.HiddenInput(),
                'userrelated':forms.HiddenInput(),
              }
    forma = forma.advanced_form_to_model(modelo=modelo,excludes=excludes,widgets=widgets)
    forma = forma(instance = c)
    extras = ExtraFields.objects.all().order_by('ordering') 
    extraform = DynamicForm()
    extraform = extraform.from_model(extras)
    initializing = MetaContact.objects.filter(contact_meta=idc)
        
    contactform = []
    addsform = []
    rsform = []
    emailform = []

    for em in c.emaillist_set.all():
        initial = {'whereis':em.typeofemail,'contactinfo':em.email,'typeof':'email'}
        emailform +=[Formcontact(initial=initial)]
    for ini in initializing:
        if ini.metavalue:
            if ini.asignetstring in 'contact':
                exte = None
                metavalues = ini.metavalue.split('|')
                try: 
                    extencion = metavalues[0].split(' ext ')
                    try:
                        contactinfo = extencion[0]
                        exte = extencion[1]
                    except:
                        contactinfo = metavalues[0]
                    whereis = metavalues[1]

                except: 
                    contactinfo = None
                    whereis = None
                initial = {'typeof':ini.metastring,'contactinfo':contactinfo,'whereis':whereis,'ext':exte}
                contactform += [Formcontact(initial=initial)] 
            if ini.asignetstring in 'redsocial':
                initial = {'redsociallist':ini.metastring,'redsocial':u'%s'%ini.metavalue}
                rsform += [Formcontact(initial=initial)] 
            if ini.asignetstring in 'direccion':
                initial = {'whereisdir':ini.metastring,'direccion':u'%s'%ini.metavalue}
                addsform += [Formcontact(initial=initial)] 
    
    fd = [Formcontact()]
    if len(addsform)==0:
         addsform = fd
    if len(contactform)==0 and not emailform:
            contactform = fd
    if len(rsform)==0:
            rsform = fd

    photo = ContactFiles.objects.filter(pk=c.photoc)
    if photo:
        photo = photo[0].contact_file
    return render(request,'forms/eform.html',
                              {'lista':'forms_list',
                              'extraform':extraform,
                              'forma':forma,
                              'cform':contactform,
                              'rsform':rsform,
                              'addsform':addsform,
                              'emailform':emailform,
                              'idc':idc,
                              'photo':photo,
                              'states':states,'extend':INDEX_EXTEND})

@login_required(login_url='/')
def delcontact(request,idc=None):
    context = RequestContext(request)
    contact = Contact.objects.get(pk=idc)
    contact.status=2
    contact.save()
    return redirect('/mycontacts')
    return render_to_response('forms/edt_contact.html',{'lista':forms_list,'contact':contact},context)


@login_required(login_url='/')
def mycontacts(request):
    context = RequestContext(request)
    args = Q()
    kwargs = {}
    args.add(~Q(status=2),Q.AND)
    nombre = request.POST.get('namec',None)
    tags = request.POST.getlist('tag')
    estado = request.POST.get('estado',None)
    delomun = request.POST.get('delomun',None)
    emailstr = request.POST.get('email',None)
    if nombre:
        palabrotas = nombre.split()
        for n in palabrotas:
            args.add((Q(namec__icontains=n) | Q(lastname__icontains=n)),Q.AND)
    if tags:
        for t in tags:
            args.add(Q(tags__icontains='%s'%(t)),Q.AND)
    if estado and not delomun:
            args.add(Q(estado__icontains='%s'%(estado)),Q.AND)
    if delomun: 
            args.add(Q(delomun__icontains='%s'%(delomun)),Q.AND)
    if emailstr:
            ilista = EmailList.objects.filter(email__icontains='%s'%(emailstr)).values('contacto')
            ids = [(x['contacto']) for x in ilista]
            args.add(Q(id__in=ids),Q.AND)
    contacts = Contact.objects.filter(*[args],**kwargs).order_by('-date_create')
    abc = abc_tag_list()
    states = Sepomex.objects.all().distinct('estado')
    city = None
    locations = [states,city]    
    formdata = request.POST.copy()
    try: del formdata['csrfmiddlewaretoken']
    except: pass
    try: del formdata['tag']
    except: pass

    #PAGINACION
    set_page = request.GET.get('page',1) 
    contact_list = contacts
    paginator = Paginator(contact_list,10)
    contacts = paginator.page(set_page)
    
    return render_to_response('mycontacts.html',
                            {'contacts':contacts,
                            'abc':abc,'formdata':formdata,
                            'location':locations,'tags':tags,
                            },context)


# DATAIL CONTACT*******************************************************************************************


@login_required(login_url='/')
def detailcontact(request,idc=None):
    context = RequestContext(request)
    c = Contact.objects.get(pk=idc)    
    # PARAMS TO TASK MODULE
    if c:
        y,m = datetime.datetime.now().year,datetime.datetime.now().month
        tasks = {'typeof':'contact',
                 'asignedto':c.pk,'y':y,'m':m,'py':y-1,'pm':m-1,'ny':y+1,'nm':m+1}

    return render(request,'detailcontact.html',{'c':c,'tasks':tasks})

# SEARCH RESULTS *********************************************************************************************
def resultados(request):
    context = RequestContext(request)
    wordss = request.POST.get('words',None)
    #kwargs = {'status':1}
    kwargs = {} #~Q(status=2)
    ekwargs = {} 
    args = Q()
    eargs = Q()
    args.add(~Q(status=2),Q.AND)
    namec = request.POST.get('namec',None)
    estado = request.POST.get('estado',None)
    try:
        tag = request.POST.getlist('tag')
        tag = sorted(set(tag)) 
    except:
        tag = None
    if namec:
        kwargs['namec__icontains']='%s'%(namec)
    if tag:
        for t in tag:
            args.add(Q(tags__icontains=u'%s'%(t)),Q.AND)
            eargs.add(Q(tags__icontains=u'%s'%(t)),Q.AND)
            #kwargs['tags__icontains']="%s"%(t)
    formdata = request.POST.copy()
    states = Sepomex.objects.all().distinct('estado')
    if estado:
        city = Sepomex.objects.filter(estado=estado).distinct('municipio')
        kwargs['estado']=estado
    else:
        city = None
    
    locations = [states,city]    
    
    if wordss:
        args = Q(namec__icontains=wordss)
        eargs = Q(nameac__icontains=wordss)
        em = EmailList.objects.filter(email__icontains=wordss)
        args.add(Q(id__in=em),Q.OR)
        args.add(Q(metacontact__metavalue__icontains=u'%s'%wordss),Q.OR)
        eargs.add(Q(metaentity__metavalue__icontains=u'%s'%wordss),Q.OR) 
        
        eargs.add(Q(emailacoount__icontains=wordss),Q.OR)
        args.add(Q(tags__icontains=u'%s'%(wordss)),Q.OR)
        eargs.add(Q(tags__icontains=u'%s'%(wordss)),Q.OR)
    response = []
    c = Contact.objects.filter(*[args],**kwargs).distinct('id')
    o = Entity.objects.filter(*[eargs],**ekwargs).distinct('id')
    for item in c:
        response.append({'name':item.namec,'type':'Contacto','photoimg':item.photoimg,'link':'/detailcontact/'+str(item.pk)+'/',
                         'state':item.estado,'me':item,'phones':None,'lat':item.lat,'lon':item.lon,'icon':'contact.png'
                        })
    if o:
        for item in o:
            response.append({'name':item.nameac,'type':'Organización','photoimg':item.photoimg,'link':'/detailentity/'+str(item.pk)+'/',
                          'me':item,'phones':None,'lat':item.latac,'lon':item.lonac,'icon':'entity.png'
                        })
    abc=abc_tag_list()
    try: del formdata['csrfmiddlewaretoken']
    except: pass
    try: del formdata['tag']
    except: pass

    #PAGINACION
    set_page = request.GET.get('page',1) 
    listing = response
    paginator = Paginator(listing,10)
    response = paginator.page(set_page)
 

    return render_to_response('myresults.html',
                             {'response':response,
                              'abc':abc,
                              'formdata':formdata,
                              'tags':tag,
                              'location':locations},
                              context)

def logoff(request):
    logout(request)
    return redirect('/')

def userreg(request):
    INDEX_EXTEND = 'base3.html'
    states=None
    modelo = Contact
    modeluser = User
    forma = FormCreator()
    excludes = ('usercreator','userrelated_id','status','date_create','phones','tags')
    widgets = {
                'estado':forms.Select(choices=ESTADOS,attrs={'class':'sensao'}),
                'delomun':forms.Select(),
                'colonia':forms.Select(),
                'cp':forms.Select(),
                'lat':forms.HiddenInput(),
                'lon':forms.HiddenInput(),
                'photoc':forms.HiddenInput()
              }
    formuser = forma.advanced_form_to_model(modelo=modeluser)
    forma = forma.advanced_form_to_model(modelo=modelo,excludes=excludes,widgets=widgets)
    extras = ExtraFields.objects.all().order_by('ordering') 
    extraform = DynamicForm()
    extraform = extraform.from_model(extras)
    contactform = [Formcontact()]
    rsform = [Formcontact()]
    addsform = [Formcontact()]
    return render(request,'forms/user_form.html',
                              {'lista':'forms_list',
                              'extraform':extraform,
                              'formuser':formuser,
                              'forma':forma,
                              'cform':contactform,
                              'rsform':rsform,
                              'addsform':addsform,
                              'states':states,'extend':INDEX_EXTEND})


@login_required(login_url='/')
def edtaccount(request):
    idc = request.GET.get('pk',None)
    if idc:
        c = Contact.objects.get(pk=idc)
    else:
        return HttpResponse('You need an ID')

    INDEX_EXTEND = 'base2.html'
    context = RequestContext(request)
    states=None
    modelo = Contact
    modeluser = User
    forma = FormCreator()
    formuser = forma.advanced_form_to_model(modelo=modeluser)
    excludes = ('status','date_create','phones','tags')
    municipios = Sepomex.objects.filter(estado=c.estado).distinct('municipio').values('municipio')
    colonias = Sepomex.objects.filter(municipio=c.delomun).distinct('asentamiento').values('asentamiento')
    cps = Sepomex.objects.filter(asentamiento=c.colonia,
                                 municipio=c.delomun,
                                 estado=c.estado).distinct('codigo_postal').values('codigo_postal')
    
    MUNICIPIO = [('','----seleccione-----------')]
    COLONIA = [('','----seleccione-----------')]
    CP = [('','----seleccione-----------')]
    MUNICIPIO += [(x['municipio'],x['municipio']) for x in municipios]
    COLONIA += [(x['asentamiento'],x['asentamiento']) for x in colonias]
    CP += [(x['codigo_postal'],x['codigo_postal']) for x in cps]
    widgets = {
                'estado':forms.Select(choices=ESTADOS,attrs={'class':'sensao'}),
                'delomun':forms.Select(choices=MUNICIPIO),
                'colonia':forms.Select(choices=COLONIA),
                'cp':forms.Select(choices=CP),
                'lat':forms.HiddenInput(),
                'lon':forms.HiddenInput(),
                'photoc':forms.HiddenInput(),
                'usercreator':forms.HiddenInput(),
                'userrelated':forms.HiddenInput(),
              }
    forma = forma.advanced_form_to_model(modelo=modelo,excludes=excludes,widgets=widgets)
    forma = forma(instance = c)
    extras = ExtraFields.objects.all().order_by('ordering') 
    extraform = DynamicForm()
    extraform = extraform.from_model(extras)
    initializing = MetaContact.objects.filter(contact_meta=idc)
        
    contactform = []
    addsform = []
    rsform = []
    emailform = []

    for em in c.emaillist_set.all():
        initial = {'whereis':em.typeofemail,'contactinfo':em.email,'typeof':'email'}
        emailform +=[Formcontact(initial=initial)]
    for ini in initializing:
        if ini.metavalue:
            if ini.asignetstring in 'contact':
                exte = None
                metavalues = ini.metavalue.split('|')
                try: 
                    extencion = metavalues[0].split(' ext ')
                    try:
                        contactinfo = extencion[0]
                        exte = extencion[1]
                    except:
                        contactinfo = metavalues[0]
                    whereis = metavalues[1]

                except: 
                    contactinfo = None
                    whereis = None
                initial = {'typeof':ini.metastring,'contactinfo':contactinfo,'whereis':whereis,'ext':exte}
                contactform += [Formcontact(initial=initial)] 
            if ini.asignetstring in 'redsocial':
                initial = {'redsociallist':ini.metastring,'redsocial':u'%s'%ini.metavalue}
                rsform += [Formcontact(initial=initial)] 
            if ini.asignetstring in 'direccion':
                initial = {'whereisdir':ini.metastring,'direccion':u'%s'%ini.metavalue}
                addsform += [Formcontact(initial=initial)] 
    
    fd = [Formcontact()]
    if len(addsform)==0:
         addsform = fd
    if len(contactform)==0 and not emailform:
            contactform = fd
    if len(rsform)==0:
            rsform = fd

    photo = ContactFiles.objects.filter(pk=c.photoc)
    if photo:
        photo = photo[0].contact_file
    return render(request,'forms/user_form.html',
                              {'lista':'forms_list',
                              'extraform':extraform,
                              'forma':forma,
                              'cform':contactform,
                              'rsform':rsform,
                              'addsform':addsform,
                              'emailform':emailform,
                              'idc':idc,
                              'photo':photo,
                              'states':states,'extend':INDEX_EXTEND})


