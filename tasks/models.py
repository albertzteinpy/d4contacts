# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
import simplejson


CHOICES_TASK = (('0','el día del evento'),
                ('1','1 día antes'),
                ('2','2 dias antes'),
)


class Category(models.Model):
    colorful = models.CharField(u'color',max_length=12,default='#04A598')
    categoryname = models.CharField(u'Categoría',max_length=250)
    def __unicode__(self):
        return u'%s'%self.categoryname


class Tasking(models.Model):
    title_task = models.CharField(u'Título',max_length=200)
    when = models.DateField(u'¿Cuándo agregar tarea?')
    categoria = models.ForeignKey(Category,help_text='Agregar Categoría',verbose_name="Seleccione una categoría")
    anytime = models.TimeField(u'¿Horario?',blank=True,null=True)
    asigned_string = models.CharField(max_length=100)
    asignedto = models.IntegerField(blank=True,null=True)
    date_create = models.DateTimeField(auto_now=True)
    reminder = models.CharField(max_length=100,choices=CHOICES_TASK)
    mail_to_send = models.CharField(max_length=200,blank=True,null=True)
    note = models.TextField(blank=True,null=True)
    taskstatus = models.BooleanField(default=0)
    creatore = models.ForeignKey(User)


class Watcher(models.Model):
    taskid = models.ForeignKey(Tasking)
    wathcer = models.TextField()

