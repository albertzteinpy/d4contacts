from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import *


urlpatterns = patterns('tasks.views',
    #url(r'^calendar','calendar',name='calendar'),
    url(r'^taskform$','taskform',name='taskform'),
    url(r'^addtask$','addtask',name='addtask'),
    url(r'^mytasks$','mytasks',name='mytasks'),
    url(r'^addcat$','addcat',name='addcat'),
    url(r'^deltask$','deltask',name='deltask'),
)


