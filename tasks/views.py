# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response,render
import simplejson
from appentity.models import * 
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
from django.contrib.auth import logout
import string as STrg
from appcontact.views import abc_tag_list 
from django.db.models import Q
from calendar import HTMLCalendar
from datetime import date
import datetime as dtf
from tasks.models import *
from contactmanager.forms import *


class WorkCalendar(HTMLCalendar):
    def __init__(self,tasks):
        super(WorkCalendar,self).__init__()
    def formatday(self, day, weekday):
        if day != 0:
            cssclass = self.cssclasses[weekday]
            if date.today() == date(self.year, self.month, day):
                cssclass += ' today' 
            return self.day_cell(cssclass, day)
        return self.day_cell('noday', '&nbsp;')

    def formatmonth(self, year, month):
        self.year, self.month = year, month
        return super(WorkCalendar, self).formatmonth(year, month)

    def group_by_day(self, workouts):
        field = lambda workout: workout.performed_at.day
        return dict(
            [(day, list(items)) for day, items in groupby(workouts, field)]
        )

    def day_cell(self, cssclass, body):
        thap = '#%s/%s/%s' %(self.month,body,self.year)
        return '<td class="%s"><a href="%s" class="addingdate inline_blocke">%s</a></td>' % (cssclass,thap, body)



@login_required(login_url='/')
def taskform(request):
    context = RequestContext(request)
    idedition = request.GET.get('pk',None)
    try: instanced = Tasking.objects.get(pk=idedition)
    except: instanced = None
    args = {}
    forma = FormCreator()
    modelo = Tasking
    asigned = request.GET.get('asignedto',request.user.pk)
    asignedstr = request.GET.get('asgstr','blankTask')
    widgets = {
                'asigned_string':forms.HiddenInput(),
                'creatore':forms.HiddenInput(),
                'taskstatus':forms.HiddenInput(),
                'mail_to_send':forms.HiddenInput(),
                'asignedto':forms.HiddenInput(),
                'anytime':forms.HiddenInput(),
                'note':forms.HiddenInput(),
              }
    initial = {'asigned_string':asignedstr,
               'creatore':request.user,
               'asignedto':asigned,
               'reminder':0,
                } 
    forma = forma.advanced_form_to_model(modelo=modelo,widgets=widgets)
    forma = forma(initial=initial,instance=instanced)
    if request.is_ajax():
            INDEX_EXTEND = 'blank.html'
    else:
        INDEX_EXTEND = 'base2.html'
    
    args['extend']=INDEX_EXTEND
    args['forma']=forma
    args['idtask']=idedition
    return render_to_response('taskform.html',args,context)


@login_required(login_url='/')
def addtask(request):
    context = RequestContext(request)
    response = {}
    ide = request.POST.get('id',None)
    try: 
        instanced = Tasking.objects.get(pk=ide)
    except:
        instanced = None
    forma = FormCreator()
    modelo = Tasking
    data = request.POST.copy()
    forma = forma.form_to_model(modelo)
    forma = forma(data,instance=instanced)
    if forma.is_valid():
        forma.save()
        response['saved']=True
        response['callback']='callbackTask'
    else:
        response = forma.errors
    return HttpResponse(simplejson.dumps(response))
    #if request.is_ajax():
    #    INDEX_EXTEND = 'blank.html'
    #else:
    #    INDEX_EXTEND = 'base2.html'
    
    
    #args['extend']=INDEX_EXTEND
    #args['forma']=forma
    
    #return render_to_response('taskform.html',args,context)

def mytasks(request):
    tasks = Tasking.objects.filter(creatore=request.user.pk)
    args = {}
    args['tasks']=tasks
    return render(request,'mytasks.html',args)


def addcat(request):
    response = {}
    forma = FormCreator()
    modelo = Category
    data = request.POST.copy()
    forma = forma.form_to_model(modelo)
    instanced = Category.objects.filter(categoryname__iexact=data['categoryname'])
    if instanced:
        instanced = instanced[0]
    else:
        instanced = None
    forma = forma(data,instance=instanced)
    if forma.is_valid():
        saved = forma.save()
        response['saved']=saved.pk
    else:
        response = forma.errors
    return HttpResponse(simplejson.dumps(response))


def deltask(request):
    response = {}
    idt = request.GET.get('pk',None)
    if idt:
        t = Tasking.objects.filter(pk=idt)
        t.delete()
        response['doit']='done'
        response['callback']='callbackdeltask'
    else:
        t = None
        response['doit']='no task id'
    return HttpResponse(simplejson.dumps(response))


