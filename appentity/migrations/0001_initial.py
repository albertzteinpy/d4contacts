# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EntityFile'
        db.create_table(u'appentity_entityfile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type_file', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('entity_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'appentity', ['EntityFile'])

        # Adding model 'Entity'
        db.create_table(u'appentity_entity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('nameac', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('emailacoount', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('extra', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('photoa', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('date_create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('cpac', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('lonac', self.gf('django.db.models.fields.FloatField')(default=0, null=True, blank=True)),
            ('latac', self.gf('django.db.models.fields.FloatField')(default=0, null=True, blank=True)),
            ('estadoa', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('delomun', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('colonia', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('tags', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'appentity', ['Entity'])

        # Adding model 'EntitySocialInfo'
        db.create_table(u'appentity_entitysocialinfo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('entity_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appentity.Entity'])),
            ('social_accounts', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'appentity', ['EntitySocialInfo'])

        # Adding model 'MetaEntity'
        db.create_table(u'appentity_metaentity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('asignetstring', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('asignetto', self.gf('django.db.models.fields.IntegerField')()),
            ('metastring', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('metavalue', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('contact_meta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appentity.Entity'])),
        ))
        db.send_create_signal(u'appentity', ['MetaEntity'])

        # Adding model 'As_Contact'
        db.create_table(u'appentity_as_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('asociation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appentity.Entity'])),
            ('contacto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appcontact.Contact'])),
            ('date_create', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'appentity', ['As_Contact'])


    def backwards(self, orm):
        # Deleting model 'EntityFile'
        db.delete_table(u'appentity_entityfile')

        # Deleting model 'Entity'
        db.delete_table(u'appentity_entity')

        # Deleting model 'EntitySocialInfo'
        db.delete_table(u'appentity_entitysocialinfo')

        # Deleting model 'MetaEntity'
        db.delete_table(u'appentity_metaentity')

        # Deleting model 'As_Contact'
        db.delete_table(u'appentity_as_contact')


    models = {
        u'appcontact.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'default': "' '", 'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'chargec': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'colonia': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'cp': ('django.db.models.fields.CharField', [], {'default': '100', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'delomun': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'extrac': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastname': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'lon': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'namec': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'photoc': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '2'}),
            'tags': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'usercreator': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'userrelated': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'appentity.as_contact': {
            'Meta': {'object_name': 'As_Contact'},
            'asociation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appentity.Entity']"}),
            'contacto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appcontact.Contact']"}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'appentity.entity': {
            'Meta': {'object_name': 'Entity'},
            'colonia': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'cpac': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'delomun': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'emailacoount': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'estadoa': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'extra': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latac': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'lonac': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'nameac': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'photoa': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tags': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'appentity.entityfile': {
            'Meta': {'object_name': 'EntityFile'},
            'entity_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type_file': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'appentity.entitysocialinfo': {
            'Meta': {'object_name': 'EntitySocialInfo'},
            'entity_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appentity.Entity']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'social_accounts': ('django.db.models.fields.TextField', [], {})
        },
        u'appentity.metaentity': {
            'Meta': {'object_name': 'MetaEntity'},
            'asignetstring': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'asignetto': ('django.db.models.fields.IntegerField', [], {}),
            'contact_meta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appentity.Entity']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metastring': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'metavalue': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['appentity']