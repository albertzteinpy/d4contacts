# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
import simplejson
from appcontact.models import Contact


class EntityFile(models.Model):
    type_file = models.CharField(max_length=200,blank=True)
    entity_file = models.FileField(upload_to='entityFiles')
    '''
        status params 
         case 0 incomplete
         case 1 Complete
         case 2 Deleted
    
    '''

class Entity(models.Model):
    user = models.ForeignKey(User)
    nameac = models.CharField(u'Nombre Org.',max_length=200)
    emailacoount = models.CharField(u'Email',max_length=100,blank=True)
    url = models.CharField(u'Web',max_length=200,blank=True,null=True)
    extra = models.TextField(blank=True,null=True)
    photoa = models.IntegerField(blank=True,null=True)
    date_create = models.DateTimeField(auto_now_add=True)
    cpac = models.CharField(u'C.P.',max_length=20,null=True,blank=True)
    lonac = models.FloatField(default=0,blank=True,null=True)
    latac = models.FloatField(default=0,blank=True,null=True)
    estadoa = models.CharField(u'Estado',max_length=200)
    delomun = models.CharField(u'Del/Municipio',max_length=200)
    colonia = models.CharField(u'Colonia',max_length=200)
    tags = models.TextField(blank=True,null=True)

    def photoimg(self):
        try:
            photoim = EntityFile.objects.get(pk=self.photoa)
        except:
            photoim = None
        return photoim

    def mysocial(self):
        myc = self.metaentity_set.filter(asignetstring='redsocial')
        return myc

    def myaddrs(self):
        myc = self.metaentity_set.filter(asignetstring='direccion')
        return myc

    def tages(self):
        if self.tags:
            l = simplejson.loads(self.tags)
        else:
            l = None
        return l
    def __unicode__(self):
        return u'%s' % self.nameac


class EntitySocialInfo(models.Model):
    entity_id = models.ForeignKey(Entity)
    social_accounts = models.TextField()

class MetaEntity(models.Model):
    asignetstring = models.CharField(max_length=200)
    asignetto = models.IntegerField()
    metastring = models.CharField(max_length=200)
    metavalue = models.CharField(max_length=200)
    contact_meta = models.ForeignKey(Entity)




class As_Contact(models.Model):
    asociation = models.ForeignKey(Entity)
    contacto = models.ForeignKey(Contact)
    date_create = models.DateTimeField(auto_now=True)
 


