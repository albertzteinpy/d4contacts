function edition_location(lat,lon)
{
    var the_place = new google.maps.LatLng(lat,lon);
    initialize(the_place);
    $('#map-canvas').show();
}
function edit_with_address(address)
{
    var the_palce = null;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address':address},function(results,status){
        if(status==google.maps.GeocoderStatus.OK){
            the_place = new google.maps.LatLng(results[0].geometry.location.k, results[0].geometry.location.B);
            initialize(the_place);
        }    
        else{
            alert('some problem with maps');    
        }
    });
}

function localizeme()
{
    var geocoder = new google.maps.Geocoder();
    var country  = 'Mexico';
    if($(this).parents('form:first').attr('id')=='addentity')
    {
        var state = $('[name=estadoa]').val(); 
        var delomun = $('[name=delomun]').val();
        var colonia = $('[name=colonia]').val();
    }
    else
    {
        var state = $('[name=estado]').val();
        var delomun = $('[name=delomun]').val();
        var colonia = $('[name=colonia]').val();
    }
    
    address = country+' '+state+' '+delomun+','+colonia;
    geocoder.geocode({'address':address},function(results,status){
        if(status==google.maps.GeocoderStatus.OK){
            var the_place = new google.maps.LatLng(results[0].geometry.location.k, results[0].geometry.location.B);
            $('#map_canvas').html('Cargando mapa, porfavor espere...');
            $('#id_lat').val(results[0].geometry.location.k);
            $('#id_lon').val(results[0].geometry.location.B);
            $('#id_latac').val(results[0].geometry.location.k);
            $('#id_lonac').val(results[0].geometry.location.B);
            initialize(the_place);
            $('#map-canvas').removeClass('blind');
        }    
        else{
            alert('some problem with maps');    
        }
    });
}
  var marker;
  var map;
  function initialize(the_place) {
        var mapOptions = {
                zoom:9,
                center: the_place
            };

            map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
            marker = new google.maps.Marker({
                        map:map,
                        draggable:true,
                        animation: google.maps.Animation.DROP,
                        position: the_place
                    });
             google.maps.event.addListener(marker, 'click', toggleBounce);
             google.maps.event.addListener(marker, 'dragend', function() 
             {
                     var pices = marker.getPosition();
                     $.each(pices,function(x,y){ 
                        $('#id_lat').val(pices.k);
                        $('#id_lon').val(pices.B);
                        $('#id_latac').val(pices.k);
                        $('#id_lonac').val(pices.B);

                    });
             });
  }

  function toggleBounce() {

        if (marker.getAnimation() != null)
        {
                marker.setAnimation(null);
        } 
        else
        {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
  }
 
